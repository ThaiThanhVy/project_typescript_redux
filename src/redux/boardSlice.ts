import {createSlice} from "@reduxjs/toolkit";
import wordList from '../word.json'

let randomWord = Math.floor(Math.random() * wordList.words.length)

const initialState = {
    board: [
        "","","","","",
        "","","","","",
        "","","","","",
        "","","","","",
        "","","","","",
        "","","","",""],

        // Lấy ra vị trí của từng cái ô
    position: 0,
    row:0,
    
    // Result
    correctWord: wordList.words[randomWord].toLocaleUpperCase()
}

export const boardSlice = createSlice({
    name: 'board',
    initialState,
    reducers: {
        setBoard: (state , action)=>{
            state.board = action.payload
        },
        incPosition: (state)=>{
            state.position++

        },
        decPosition: (state)=>{
            state.position--

        },
        incRow:(state) => {
            state.row++
        },

        // Xem thử người dùng bấm nút back hay enter
       
    }
})


export const {setBoard , incPosition ,decPosition , incRow } = boardSlice.actions

export default boardSlice.reducer;