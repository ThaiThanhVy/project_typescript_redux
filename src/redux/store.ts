// Redux toolkit
import boardSlice from './boardSlice';


import { configureStore } from "@reduxjs/toolkit";


export default configureStore({
    reducer:{
        boardSlice
    }
})