import React, { useEffect, useState } from 'react'
// Cài đặt npm i framer-motion
import {motion} from 'framer-motion'
import './Square.css'
import { useSelector } from 'react-redux'
import { rootState } from '../interface'

interface PropsSquare {
    square:string,
    squareIdx:number
}

const Square:React.FC<PropsSquare> = (props) => {
  const {square , squareIdx} = props
  // redux
  const correctWord = useSelector((state:rootState) => state.boardSlice.correctWord)
  const position = useSelector((state:rootState) => state.boardSlice.position)
  const row = useSelector((state:rootState) => state.boardSlice.row)

  // state
  const [correct , setCorrect] = useState<Boolean>(false)
  const [almost , setAlmost] = useState<Boolean>(false)
  const [wrong , setWrong] = useState<Boolean>(false)

  let currentPosition = (position - 1) % 5 

  const variants = {
    filled:() => ({
      scale: [1.2,1],
      transition: {
        duration:0.2
      }
    }),
    unfilled:() => ({
      scale: [1.2,1],
      transition: {
        duration:0.2
      }
    }),
  }

  useEffect(() => {
    if(correctWord[currentPosition] === square){
      setCorrect(true)
    } else if(!correct && square !== '' && correctWord.includes(square) ){
      setAlmost(true)
    } else if(!correct && square !== '' && !correctWord.includes(square)) {
      setWrong(true)
    }

    //Phương thức componentWillMount được thực thi trước khi giao diện được render ra màn hình ở cả server và client side.
    // Khi unmont nó sẽ set lại thanh false
    return () => {
      setCorrect(false)
      setAlmost(false)
      setWrong(false)
    }
  },[square])

  // logic Math.floor(squareIdx / 5) không thể nào > row trừ khi ta bấm Enter 
  // Cho nên biến status chỉ đc chạy khi bấm Enter

  const status:any = Math.floor(squareIdx / 5) < row && (correct ? 'correct' : almost ? 'almost' : wrong ? 'wrong' : '') 


  return (
    <motion.div animate={square ? 'filled' : 'unfilled'} variants={variants}>
    <div className='square' id={status}>{square}</div>
    </motion.div>
  )
}

export default Square