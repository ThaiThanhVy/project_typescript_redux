import React from 'react'
import { rootState } from '../interface'
import {useSelector} from 'react-redux'
import {useDispatch} from 'react-redux'
import './Key.css'
import { incPosition, setBoard } from '../../redux/boardSlice'

interface PropsKey {
    letter: string
}

const Key:React.FC<PropsKey> = (props) => {
  const board = useSelector((state:rootState) => state.boardSlice.board)
  const position = useSelector((state:rootState) => state.boardSlice.position)
  const currentRow = Math.floor(position / 5)
  const row = useSelector((state:rootState) => state.boardSlice.row)


  const dispatch = useDispatch()
    const {letter} = props
   
    const clickLetter = () => {
      // Cách tính row
      // console.log(Math.floor(position / 5))

      // nếu position > 30 thì chấm dứt không chạy nữa
      // nếu currentRow > row thì chấm dứt không chạy nữa
      if(position >= 30) return;
      if(currentRow > row) return;
      const newBoard = [...board]
      
      // bỏ position vào đây
      newBoard[position] = letter
      // console.log('newBoard[0]: ', newBoard[0]);
      dispatch(setBoard(newBoard))

      // Khi Bấm vào các phím thì tăng dần chữ lên
      dispatch(incPosition())
    }
  return (
    <div className='letter' onClick={clickLetter}>{letter}</div>
  )
}

export default Key