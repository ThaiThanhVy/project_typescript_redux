import React from 'react'
import Key from '../Key/Key';
import {useSelector} from 'react-redux'
import {useDispatch} from 'react-redux'
import { decPosition, incRow, setBoard } from '../../redux/boardSlice'
import './Keyboard.css'
import { rootState } from '../interface';
import wordList from '../../word.json'

const Keyboard:React.FC = () => {

    const board = useSelector((state:rootState) => state.boardSlice.board)
    const position = useSelector((state:rootState) => state.boardSlice.position)
    const row = useSelector((state:rootState) => state.boardSlice.row)
    const correctWord = useSelector((state:rootState) => state.boardSlice.correctWord)
    const dispatch = useDispatch()

    const rows: string[] = [
        "q w e r t y u i o p",
        "a s d f g h j k l",
        "z x c v b n m",
      ];

      const clickBack = () => {
        if(Math.floor(position - 1) / 5 < row) return
        
        const newBoard = [...board]
        newBoard[position - 1] = "";
        // Khi Bấm vào back thì nó từ dưới lên
        dispatch(decPosition())
        dispatch(setBoard(newBoard))
      }

    //   Lấy ra 5 từ được user click để validate
    let getBoard5Word: string = `${board[position-5]}${board[position-4]}${board[position-3]}${board[position-2]}${board[position-1]}`.toLowerCase();

    const allWords: string[] = wordList.words
    
    const clickEnter = () => {
        if (allWords.includes(getBoard5Word) === false) {
          alert("Invalid words");
        }
        if(allWords.includes(getBoard5Word)) {
          if (position % 5 === 0 && position !== 0) {
           dispatch(incRow());
        }
      }
        if(position === 30 && allWords.includes(getBoard5Word)) {
          alert("The word is: " + correctWord);
        }

        if(position === 30 && allWords.includes(getBoard5Word)) {
            alert("The word is: " + correctWord);
          }
      }

  return (
    <div className=''>
        {rows.map((row , idx) => {
            return (
                <div className='row'>
                    {idx === 2 ? <span onClick={clickEnter}>Enter</span> : ''}
                    {/* Dùng hàm split(" ") để tách chữa ra  */}
                    {row.split(" ").map((letter , idx) => {
                        return (
                            <div className='letter-row'>
                                <Key key={idx} letter={letter.toUpperCase()}/>
                                {letter === 'm' ? <span onClick={clickBack}>Back</span> : ''}
                            </div>
                        )
                    })}
                </div>
            )
        })}
    </div>
  )
}

export default Keyboard