interface boardState{
    board: string[]
    position:number
    row: number
    correctWord: string
}

// Tất cả các interface được định nghĩa sẽ được bỏ vào đây
export interface rootState{
    boardSlice: boardState
}