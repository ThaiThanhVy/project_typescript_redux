import React from 'react'
import Keyboard from '../Keyboard/Keyboard';
import Square from '../Square/Square';
import'./Board.css'

interface PropsBoard {
    board:string[]
}

const Board:React.FC<PropsBoard> = (props) => {

    const {board} = props
  return (
    <>
    <div className='board'>
        {board.map((square , index) => {
            return (<>
            <Square key={index} square={square} squareIdx={index}/>
            </>)
        })}
    </div>
        <div><Keyboard/></div>
    </>
    
  )
}

export default Board