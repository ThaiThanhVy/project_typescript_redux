import React from 'react'
import './Header.css'
interface Props{
    type:string,
    text:string
}

const Header: React.FC<Props> = (props) => {

    const {type , text} = props
  return (
    <div className={`header-${type}`}>{text}</div>
  )
}

export default Header