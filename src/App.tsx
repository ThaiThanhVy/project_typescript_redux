import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './Components/Header/Header';
import Board from './Components/Board/Board';
import {useSelector} from 'react-redux'
import {rootState} from './Components/interface'
import HowToPlay from './Components/HowToPlay/HowToPlay';
function App() {
  // const [board , setBoard] = useState<string[]>([
  //   "","","","","",
  //   "","","","","",
  //   "","","","","",
  //   "","","","","",
  //   "","","","","",
  //   "","","","",""])

  const board = useSelector((state:rootState) => state.boardSlice.board)

  return (
    <div className="App">
      <HowToPlay/>
      <Header type='title' text='Wordle'/>  
      <Header type='h1' text='Play'/>  
      <div className="board-container">

        {/* Chứa bảng 6*5 */}
        <Board board={board}/>
      </div>
    </div>
  );
}

export default App;
